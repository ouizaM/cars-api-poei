FROM openjdk:12-alpine
VOLUME /tmp
ADD /build/libs/*.jar    cars-api-poei.jar
EXPOSE 5000
ENTRYPOINT exec java -jar cars-api-poei.jar
